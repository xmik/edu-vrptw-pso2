import time as thetime
import os
from gavrptw.core import run_psovrptw, eval_vrptw, instance_name_to_instance_object, print_route, ind2route
import pandas as pd
import solomon
import multiprocessing


def run_one_problem(problem_name, pso_config, verbose=True):
    t0 = thetime.perf_counter()
    pop, logbook, best = run_psovrptw(instance_name=problem_name, \
                 unit_cost=pso_config.unit_cost, init_cost=pso_config.init_cost, wait_cost=pso_config.wait_cost, delay_cost=pso_config.delay_cost,
                 particle_indices_size=pso_config.particle_indices_size, pop_size=pso_config.pop_size, generations_count=pso_config.generations_count,
                 cognitive_acceleration_coefficient=pso_config.cognitive_acceleration_coefficient,
                 social_acceleration_coefficient=pso_config.social_acceleration_coefficient,
                 speed_min=pso_config.speed_min, speed_max=pso_config.speed_max)
    t1 = thetime.perf_counter()

    instance = instance_name_to_instance_object(problem_name)
    fitness, total_time_cost, total_distance_cost = eval_vrptw(
        best, instance, pso_config.unit_cost, pso_config.init_cost, pso_config.wait_cost, pso_config.delay_cost)

    route = ind2route(best, instance)
    time_elapsed = round(t1 - t0,5)
    if verbose:
        print(problem_name)
        print("Total distance cost: %s" % round(total_distance_cost,2))
        print("Vehicles count: %s" % len(route))
        print_route(route)
        print(f'Best individual encoded: {best}')
        print("Total time cost: %s" % round(total_time_cost,2))
        print("Total cost: %s" % str(round(total_distance_cost + total_time_cost,2)))
        print("Time elapsed: ", time_elapsed)  # CPU seconds elapsed (floating point)
        return len(route), round(total_distance_cost,2), time_elapsed
    else:
        return len(route), round(total_distance_cost,2), time_elapsed


# runs the function run_one_problem many times and computes the averages
def get_file_name(pop_size, generations_count,
                    cognitive_acceleration_coefficient, social_acceleration_coefficient, speed_min, speed_max):
    return "pop_" + str(pop_size )+ "-gen_" + str(generations_count) + "-cog_" +\
           str(cognitive_acceleration_coefficient) + \
           "-soc_" + str(social_acceleration_coefficient) + \
           "-speedmin_" + str(speed_min) + "-speedmax_" + str(speed_max) + ".csv"


def get_header(solomon_problems, delimiter):
    header = ""
    for solomon_problem in solomon_problems:
        header += "VC_" + solomon_problem + delimiter + \
                  "Dist_" + solomon_problem + delimiter + \
                  "Time_" + solomon_problem + delimiter
    # remove the last delimiter
    return header[:-1]


def remove_file_if_exists(file_name_mean):
    if os.path.exists(file_name_mean):
        os.remove(file_name_mean)


def aggregate_results(file_name_general, delimiter):
    results = pd.read_csv(
        file_name_general,
        sep=delimiter,
        header=0,
    )
    file_name_mean = file_name_general[:-4]+"-mean.csv"
    remove_file_if_exists(file_name_mean)
    with open(file_name_mean, 'a') as file:
        file.write(results.agg(['mean']).to_csv(sep=delimiter))

    file_name_mode = file_name_general[:-4]+"-mode.csv"
    remove_file_if_exists(file_name_mode)
    with open(file_name_mode, 'a') as file:
        file.write(results.agg(['mode']).to_csv(sep=delimiter))


class OneIterationRunner():
    def __init__(self, solomon_problems, delimiter, pso_config):
        self.solomon_problems = solomon_problems
        self.delimiter = delimiter
        self.pso_config = pso_config

    def run(self, iteration):
        result_str = ""
        for solomon_problem in self.solomon_problems:
            print("Iteration: %s, problem: %s" % (str(iteration), solomon_problem))
            vehicles_count, cost, time = run_one_problem(
                solomon_problem, self.pso_config, verbose=False)
            result_str += str(vehicles_count) + self.delimiter + str(cost) + self.delimiter + str(time) + self.delimiter
        # remove the last delimiter
        result_str = result_str[:-1]
        return result_str


class PsoConfig():
    def __init__(self, unit_cost, init_cost, wait_cost, delay_cost, particle_indices_size,
            pop_size, generations_count, cognitive_acceleration_coefficient, social_acceleration_coefficient,
            speed_min, speed_max):
        # internal evaluation parameters needed by the algorithm to compute the fitness value
        self.unit_cost = unit_cost
        self.init_cost = init_cost
        self.wait_cost = wait_cost
        self.delay_cost = delay_cost

        # this is constant for all the problems with 100 customers
        self.particle_indices_size = particle_indices_size

        # parameters for end user to change
        self.pop_size = pop_size
        self.generations_count = generations_count
        # those 2 parameters below must sum to 4
        self.cognitive_acceleration_coefficient = cognitive_acceleration_coefficient
        self.social_acceleration_coefficient = social_acceleration_coefficient
        self.speed_min = speed_min
        self.speed_max = speed_max

    def __str__(self):
        result_str = """
Parameters:
pop_size: %s
generations_count: %s
cognitive_acceleration_coefficient: %s
social_acceleration_coefficient: %s
speed_min: %s
speed_max: %s
""" % (self.pop_size, self.generations_count, self.cognitive_acceleration_coefficient,
       self.social_acceleration_coefficient, self.speed_min, self.speed_max)
        return result_str


def run_for_one_parameters_set(solomon_problems, iterations_count, delimiter, header, pso_config):
    print(pso_config)

    # compute the cost for the chosen parameters set, for many Solomon Problems, run many attempts
    file_name = os.path.join("results", "data", get_file_name(
        pso_config.pop_size, pso_config.generations_count, pso_config.cognitive_acceleration_coefficient,
                pso_config.social_acceleration_coefficient, pso_config.speed_min, pso_config.speed_max))

    t0 = thetime.perf_counter()
    runner = OneIterationRunner(solomon_problems, delimiter, pso_config)
    with multiprocessing.Pool() as pool:
        results_map = pool.map(runner.run, range(0, iterations_count))

    # save results to file
    remove_file_if_exists(file_name)
    with open(file_name, 'a') as file:
        file.write(header + "\n")
    for result in results_map:
        with open(file_name, 'a') as file:
            file.write(result + "\n")

    # for each attempt, compute the mean and mode values
    aggregate_results(file_name, delimiter)
    t1 = thetime.perf_counter()
    print("Total time elapsed: ", str(round(t1 - t0, 5)))  # CPU seconds elapsed (floating point)


# Generates 3 files for each pso config:
#   * 1 with results of PSO (vehicles count, cost, time) for each of Solomons Problems; series in columns
#   * 1 with mean value for each series (for each column)
#   * 1 with mode values for each series (for each column)
def main():
    solomon_problems = solomon.collect_solomon_problems_and_choose()
    iterations_count = 10
    delimiter = ";"
    header = get_header(solomon_problems, delimiter)

    pop_sizes_generations_counts = [
        (400, 100), (100, 400), (100, 100), (10, 400), (400, 10), (10, 100), (100, 10)
    ]
    other_configs_combinations = [
        (2.0,2.0,-3.0,3.0),
        (1.0,3.0,-3.0,3.0),
        (3.0,1.0,-3.0,3.0),
        (2.0,2.0,-1.0,1.0),
        (2.0,2.0,-10.0,10.0),
    ]
    for elem in pop_sizes_generations_counts:
        for elem2 in other_configs_combinations:
            pso_config = PsoConfig(
                unit_cost=1.0, init_cost=0.0, wait_cost=1.0, delay_cost=2.0, particle_indices_size=100,
                pop_size=elem[0], generations_count=elem[1],
                cognitive_acceleration_coefficient=elem2[0], social_acceleration_coefficient=elem2[1],
                speed_min=elem2[2], speed_max=elem2[3],
            )
            run_for_one_parameters_set(solomon_problems, iterations_count, delimiter, header, pso_config)


if __name__ == '__main__':
    main()
