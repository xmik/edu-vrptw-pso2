import os
import matplotlib.pyplot as plt
import pandas as pd
import solomon

def main():
    results_dir = os.path.join("results", "data")
    charts_dir= os.path.join("results", "charts")
    resuls_csv_files = []
    for filename in os.listdir(results_dir):
        if filename == ".placeholder":
            continue
        if not filename.endswith("-mean.csv") and not filename.endswith("-mode.csv"):
            resuls_csv_files.append(
                os.path.join(results_dir, filename))

    solomon_problems = solomon.collect_solomon_problems_and_choose()
    iterations_count = 10
    x_axis_points = range(1,iterations_count+1)

    # Generate 1 chart for a series (10 iterations) of 1 problem + mean value, so 10 points in total
    # on each chart + mean value.
    # There will be len(solomon_problems) * parameters_sets_count (35) number of such charts
    for filename in resuls_csv_files:
        data = pd.read_csv(filename, sep=';', header=0)
        data_mean = pd.read_csv(filename[:-4]+"-mean.csv", sep=';', header=0)
        parameters_set_name = filename.split('/')[-1][:-4]
        for i, sp in enumerate(solomon_problems):
            fig, axes = plt.subplots(nrows=3, figsize=(18, 12))
            # without this the labels of x axis overlaps next plot title
            plt.subplots_adjust(hspace = 0.5)
            fig.suptitle(sp + " " + parameters_set_name, fontsize=16)

            axes[0].scatter(y=data['VC_'+sp], x=x_axis_points, marker="o", label="data")
            axes[0].set_title(sp + " vehicles count")
            axes[0].set_xlabel("iteration")
            y_mean = [data_mean['VC_'+sp]] * len(x_axis_points)
            axes[0].plot(x_axis_points, y_mean, label='mean', linestyle='--');
            axes[0].legend()
            axes[0].ticklabel_format(useOffset=False)

            axes[1].scatter(y=data['Dist_'+sp], x=x_axis_points, marker="o", label="data")
            axes[1].set_title(sp + " distance")
            axes[1].set_xlabel("iteration")
            y_mean = [data_mean['Dist_'+sp]] * len(x_axis_points)
            axes[1].plot(x_axis_points, y_mean, label='mean', linestyle='--');
            axes[1].legend()
            axes[1].ticklabel_format(useOffset=False)

            axes[2].scatter(y=data['Time_'+sp], x=x_axis_points, marker="o", label="data")
            axes[2].set_title(sp + " computation time")
            axes[2].set_xlabel("iteration")
            y_mean = [data_mean['Time_'+sp]] * len(x_axis_points)
            axes[2].plot(x_axis_points, y_mean, label='mean', linestyle='--');
            axes[2].legend()
            axes[2].ticklabel_format(useOffset=False)

            plt.savefig(os.path.join(charts_dir, sp + "-" + parameters_set_name + ".png"))
            #plt.show()
            print("Done for: " + sp)
            plt.close(fig)


    # Generate 1 chart for 1 solomon problem (using its mean values) for each parameters set, so 35 values in total
    # on each chart.
    # Number of such charts: len(solomon_problems).
    for sp in solomon_problems:
        sp_data = []
        for i, filename in enumerate(resuls_csv_files):
            data_mean = pd.read_csv(filename[:-4]+"-mean.csv", sep=';', header=0)
            data_mean_dist = data_mean['Dist_'+sp].values[0]
            data_mean_time = data_mean['Time_'+sp].values[0]
            data_mean_vc = data_mean['VC_'+sp].values[0]
            parameters_set_name = filename.split('/')[-1][:-4]
            sp_data.append((i, parameters_set_name, data_mean_vc, data_mean_dist, data_mean_time))

        fig, axes = plt.subplots(nrows=3, figsize=(18, 36))
        fig.suptitle(sp, fontsize=16)
        for i, txt in enumerate(sp_data):
            axes[0].scatter(sp_data[i][0], sp_data[i][2])
            axes[0].annotate(sp_data[i][1], (sp_data[i][0], sp_data[i][2]))
            axes[0].set_xlabel("parameters set")
            axes[0].set_title(sp + " vehicles count")

            axes[1].scatter(sp_data[i][0], sp_data[i][3])
            axes[1].annotate(sp_data[i][1], (sp_data[i][0], sp_data[i][3]))
            axes[1].set_xlabel("parameters set")
            axes[1].set_title(sp + " distance")

            axes[2].scatter(sp_data[i][0], sp_data[i][4])
            axes[2].annotate(sp_data[i][1], (sp_data[i][0], sp_data[i][4]))
            axes[2].set_xlabel("parameters set")
            axes[2].set_title(sp + " computation time")

        plt.savefig(os.path.join(charts_dir, sp + ".png"))
        print("Done for: " + sp)
        plt.close(fig)


if __name__ == '__main__':
    main()