import os



def collect_solomon_problems():
    """Returns the collection of all downloaded Solomon Problems."""
    solomon_problems = []
    for filename in os.listdir('data/json'):
        solomon_problem = filename.split(".")[0]
        solomon_problems.append(solomon_problem)
    solomon_problems.sort()
    return solomon_problems


def collect_solomon_problems_and_choose():
    """Returns the collection of chosen 12 downloaded Solomon Problems."""
    solomon_problems = collect_solomon_problems()

    chosen_solomon_problems = [
        'C101', 'C105',
        'C201', 'C205',
        'R101', 'R105',
        'R201', 'R205',
        'RC101', 'RC105',
        'RC201', 'RC205',
    ]
    for prob in chosen_solomon_problems:
        if prob not in solomon_problems:
            raise ValueError("%s not downloaded", prob)

    return chosen_solomon_problems