import gavrptw.core as core
import deap as deap


def test_update_particle():
    deap.creator.create("FitnessMax", deap.base.Fitness, weights=(1.0,))
    deap.creator.create("Particle", list, fitness=deap.creator.FitnessMax, speed=list,
                   smin=None, smax=None, best=None)
    toolbox = deap.base.Toolbox()
    toolbox.register("particle", core.generate, size=3, pmin=1, pmax=3, smin=-3, smax=3)
    toolbox.register("population", deap.tools.initRepeat, list, toolbox.particle)
    pop = toolbox.population(n=1)

    the_particle = pop[0]
    the_particle.best = the_particle
    speed_before_update = list(the_particle.speed)

    core.updateParticle(the_particle, the_particle, 2.0, 2.0)
    speed_after_update = list(the_particle.speed)
    # assert speed_before_update == speed_after_update

    assert 1 in the_particle
    assert 2 in the_particle
    assert 3 in the_particle

def test_update_particle_many():
    deap.creator.create("FitnessMax", deap.base.Fitness, weights=(1.0,))
    deap.creator.create("Particle", list, fitness=deap.creator.FitnessMax, speed=list,
                   smin=None, smax=None, best=None)
    toolbox = deap.base.Toolbox()
    toolbox.register("particle", core.generate, size=10, pmin=1, pmax=10, smin=-3, smax=3)
    toolbox.register("population", deap.tools.initRepeat, list, toolbox.particle)
    pop = toolbox.population(n=1)

    the_particle = pop[0]
    the_particle.best = the_particle
    speed_before_update = list(the_particle.speed)

    core.updateParticle(the_particle, the_particle, 2.0, 2.0)
    speed_after_update = list(the_particle.speed)
    # assert speed_before_update == speed_after_update

    assert 1 in the_particle
    assert 2 in the_particle
    assert 3 in the_particle
    assert 4 in the_particle
    assert 5 in the_particle
    assert 6 in the_particle
    assert 7 in the_particle
    assert 8 in the_particle
    assert 9 in the_particle
    assert 10 in the_particle
    assert 11 not in the_particle


def test_find_repeats():
    myarr = [1,2,3,3,4]
    assert core.find_repeats(myarr) == [3]


def test_find_repeats2():
    myarr = [1,2,3,3,4,2,6]
    assert core.find_repeats(myarr) == [3,2]

