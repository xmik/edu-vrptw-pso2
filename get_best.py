import os
import pandas as pd
import solomon


def main():
    results_dir = os.path.join("results", "data")
    resuls_csv_files = []
    for filename in os.listdir(results_dir):
        if filename == ".placeholder":
            continue
        if not filename.endswith("-mean.csv") and not filename.endswith("-mode.csv"):
            resuls_csv_files.append(
                os.path.join(results_dir, filename))


    solomon_problems = solomon.collect_solomon_problems_and_choose()
    print("Problem;NV;Dist;ParametersSet")
    for sp in solomon_problems:
        smallest_dist = 999999999
        vc_best = -1
        parameters_set_name_best = None
        for i, filename in enumerate(resuls_csv_files):
            data_mean = pd.read_csv(filename[:-4]+"-mean.csv", sep=';', header=0)
            data_mean_dist = data_mean['Dist_'+sp].values[0]
            data_mean_time = data_mean['Time_'+sp].values[0]
            data_mean_vc = data_mean['VC_'+sp].values[0]
            if data_mean_dist < smallest_dist:
                smallest_dist = data_mean_dist
                vc_best = data_mean_vc
                parameters_set_name_best = filename.split('/')[-1][:-4]
        print(sp + ";" + str(vc_best) + ";" + str(smallest_dist) + ";" + str(parameters_set_name_best))


if __name__ == '__main__':
    main()