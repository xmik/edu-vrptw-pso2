# -*- coding: utf-8 -*-

'''sample_C204.py'''

import random
from gavrptw.core import run_gavrptw, run_psovrptw


def main():
    '''main()'''
    random.seed(64)

    instance_name = 'C204'

    unit_cost = 8.0
    init_cost = 100.0
    wait_cost = 1.0
    delay_cost = 1.5

    customers_count = 100
    particles_count_in_population = 400
    max_generations = 1000

    export_csv = True

    run_psovrptw(instance_name=instance_name, \
                 unit_cost=unit_cost, init_cost=init_cost, wait_cost=wait_cost, delay_cost=delay_cost, \
                 particle_indices_size=customers_count, pop_size=particles_count_in_population, generations_count=max_generations, \
                 export_csv=export_csv)


if __name__ == '__main__':
    main()
