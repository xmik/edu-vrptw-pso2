# -*- coding: utf-8 -*-

'''gavrptw/core.py'''

import os
import io
import random
import operator
import math

from csv import DictWriter
from deap import base, creator, tools
from . import BASE_DIR
from .utils import make_dirs_for_file, exist, load_instance
import numpy as numpy


def ind2route(individual, instance):
    '''gavrptw.core.ind2route(individual, instance)'''
    route = []
    vehicle_capacity = instance['vehicle_capacity']
    depart_due_time = instance['depart']['due_time']
    # Initialize a sub-route
    sub_route = []
    vehicle_load = 0
    elapsed_time = 0
    last_customer_id = 0
    for customer_id in individual:
        # Update vehicle load
        demand = instance[f'customer_{customer_id}']['demand']
        updated_vehicle_load = vehicle_load + demand
        # Update elapsed time
        service_time = instance[f'customer_{customer_id}']['service_time']
        return_time = instance['distance_matrix'][customer_id][0]
        updated_elapsed_time = elapsed_time + \
            instance['distance_matrix'][last_customer_id][customer_id] + service_time + return_time
        # Validate vehicle load and elapsed time
        if (updated_vehicle_load <= vehicle_capacity) and (updated_elapsed_time <= depart_due_time):
            # Add to current sub-route
            sub_route.append(customer_id)
            vehicle_load = updated_vehicle_load
            elapsed_time = updated_elapsed_time - return_time
        else:
            # Save current sub-route
            route.append(sub_route)
            # Initialize a new sub-route and add to it
            sub_route = [customer_id]
            vehicle_load = demand
            elapsed_time = instance['distance_matrix'][0][customer_id] + service_time
        # Update last customer ID
        last_customer_id = customer_id
    if sub_route != []:
        # Save current sub-route before return if not empty
        route.append(sub_route)
    return route


def print_route(route, merge=False):
    '''gavrptw.core.print_route(route, merge=False)'''
    route_str = '0'
    sub_route_count = 0
    for sub_route in route:
        sub_route_count += 1
        sub_route_str = '0'
        for customer_id in sub_route:
            sub_route_str = f'{sub_route_str} - {customer_id}'
            route_str = f'{route_str} - {customer_id}'
        sub_route_str = f'{sub_route_str} - 0'
        if not merge:
            print(f'  Vehicle {sub_route_count}\'s route: {sub_route_str}')
        route_str = f'{route_str} - 0'
    if merge:
        print(route_str)


def eval_vrptw(individual, instance, unit_cost=1.0, init_cost=0, wait_cost=0, delay_cost=0):
    route = ind2route(individual, instance)
    total_cost = 0
    total_time_cost = 0
    total_distance_cost = 0
    max_vehicles_count = instance['max_vehicle_number']
    if len(route) > max_vehicles_count:
        # too many vehicles used, let's set a very big cost and exit
        total_cost = 100000
        total_time_cost = 100000
        total_distance_cost = 100000
    else:
        for sub_route in route:
            sub_route_time_cost = 0
            sub_route_distance = 0
            elapsed_time = 0
            last_customer_id = 0
            for customer_id in sub_route:
                # Calculate section distance
                distance = instance['distance_matrix'][last_customer_id][customer_id]
                # Update sub-route distance
                sub_route_distance = sub_route_distance + distance

                # Calculate time cost
                arrival_time = elapsed_time + distance
                time_cost = wait_cost * \
                    max(instance[f'customer_{customer_id}']['ready_time'] - arrival_time, 0) + \
                    delay_cost * \
                    max(arrival_time - instance[f'customer_{customer_id}']['due_time'], 0)
                total_time_cost += time_cost
                # Update sub-route time cost
                sub_route_time_cost = sub_route_time_cost + time_cost

                # Update elapsed time
                elapsed_time = arrival_time + \
                    instance[f'customer_{customer_id}']['service_time']

                # Update last customer ID
                last_customer_id = customer_id
            # Calculate transport cost
            sub_route_distance = sub_route_distance + instance['distance_matrix'][last_customer_id][0]
            total_distance_cost += sub_route_distance
            sub_route_transport_cost = init_cost + unit_cost * sub_route_distance
            # Obtain sub-route cost
            sub_route_cost = sub_route_time_cost + sub_route_transport_cost
            # Update total cost
            total_cost = total_cost + sub_route_cost

    # Later on we choose FitnessMax, so we will maximize the fitness.
    # Since we now have total_cost, which we want to minimize,
    # we have to transform it into e.g. 1.0/total_cost.
    fitness = 1.0 / total_cost
    return (fitness, total_time_cost, total_distance_cost)


def cx_partialy_matched(ind1, ind2):
    '''gavrptw.core.cx_partialy_matched(ind1, ind2)'''
    size = min(len(ind1), len(ind2))
    cxpoint1, cxpoint2 = sorted(random.sample(range(size), 2))
    temp1 = ind1[cxpoint1:cxpoint2+1] + ind2
    temp2 = ind1[cxpoint1:cxpoint2+1] + ind1
    ind1 = []
    for gene in temp1:
        if gene not in ind1:
            ind1.append(gene)
    ind2 = []
    for gene in temp2:
        if gene not in ind2:
            ind2.append(gene)
    return ind1, ind2


def mut_inverse_indexes(individual):
    '''gavrptw.core.mut_inverse_indexes(individual)'''
    start, stop = sorted(random.sample(range(len(individual)), 2))
    individual = individual[:start] + individual[stop:start-1:-1] + individual[stop+1:]
    return (individual, )

def generate(size, pmin, pmax, smin, smax):
    # generate random non-repetitive integers between pmin to pmax
    ascending_numbers = range(pmin, pmax+1)
    randomly_shuffled = list(ascending_numbers)
    random.shuffle(randomly_shuffled)
    part = creator.Particle(randomly_shuffled)
    part.speed = [random.uniform(smin, smax) for _ in range(size)]
    part.smin = smin
    part.smax = smax
    return part

def find_repeats(numbers):
    repeated = []
    seen = []

    for number in numbers:
        if number in seen:
            repeated.append(number)
        else:
            seen.append(number)
    return repeated


# We have to use discrete space, because customers' numbers are integers.
# Using the particle speed, we get new particle values which are floats.
# We have to transform these floats to integers (and we also have to keep the order).
def updateParticle(part, best, phi1, phi2):
    u1 = (random.uniform(0, phi1) for _ in range(len(part)))
    u2 = (random.uniform(0, phi2) for _ in range(len(part)))
    # the particle's position
    v_u1 = map(operator.mul, u1, map(operator.sub, part.best, part))
    # the swarm's best known  position
    v_u2 = map(operator.mul, u2, map(operator.sub, best, part))
    part.speed = list(map(operator.add, part.speed, map(operator.add, v_u1, v_u2)))
    for i, speed in enumerate(part.speed):
        # adjust minimum speed if necessary
        if abs(speed) < part.smin:
            part.speed[i] = math.copysign(part.smin, speed)
            # adjust maximum speed if necessary
        elif abs(speed) > part.smax:
            part.speed[i] = math.copysign(part.smax, speed)
    # e.g. part is [1,2,2,3,4]
    part[:] = list(map(operator.add, part, part.speed))
    # e.g. repeated is [2]
    repeated = find_repeats(list(part[:]))

    no_repetitions = []
    if len(repeated) == 0:
        no_repetitions = part[:]
    else:
        # add some randomness
        for number in part:
            if number in repeated:
                no_repetitions.append(number + random.uniform(-0.4,0.4))
            else:
                no_repetitions.append(number)

    # sort the customers inside the particle, so that we can later know
    # which customer (as float here) represents an integer customer
    newlist = sorted(list(no_repetitions), key=float, reverse=False)

    new_part_list = []
    for customer in no_repetitions:
        index = newlist.index(customer)
        # +1 because we need customers from 1 to max customers and
        # the list is itemized from 0
        new_part_list.append((index+1))

    part[:] = new_part_list
    # we might add some verification, if len(repeated) != 0,
    # then throw an error
    # repeated = find_repeats(list(part[:]))


def instance_name_to_instance_object(instance_name, customize_data=False):
    if customize_data:
        json_data_dir = os.path.join(BASE_DIR, 'data', 'json_customize')
    else:
        json_data_dir = os.path.join(BASE_DIR, 'data', 'json')
    json_file = os.path.join(json_data_dir, f'{instance_name}.json')
    instance = load_instance(json_file=json_file)
    if instance is None:
        raise ValueError("Instance was None")
    return instance


# runs the algorithm, prints nothing
def run_psovrptw(instance_name, unit_cost, init_cost, wait_cost, delay_cost, particle_indices_size, pop_size, \
                 generations_count, cognitive_acceleration_coefficient, social_acceleration_coefficient,
                 speed_min=-3, speed_max=3,
                 customize_data=False):
    if cognitive_acceleration_coefficient + social_acceleration_coefficient != 4:
        raise ValueError("cognitive_acceleration_coefficient + social_acceleration_coefficient != 4")

    instance = instance_name_to_instance_object(instance_name, customize_data)

    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Particle", list, fitness=creator.FitnessMax, speed=list,
                   smin=None, smax=None, best=None)

    # https://deap.readthedocs.io/en/master/examples/pso_basic.html
    toolbox = base.Toolbox()
    toolbox.register("particle", generate, size=particle_indices_size, pmin=1, pmax=particle_indices_size, smin=speed_min, smax=speed_max)
    toolbox.register("population", tools.initRepeat, list, toolbox.particle)
    toolbox.register("update", updateParticle, phi1=cognitive_acceleration_coefficient, phi2=social_acceleration_coefficient)
    # toolbox.register("evaluate", benchmarks.h1)
    toolbox.register('evaluate', eval_vrptw, instance=instance, unit_cost=unit_cost, \
        init_cost=init_cost, wait_cost=wait_cost, delay_cost=delay_cost)


    pop = toolbox.population(n=pop_size)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    logbook = tools.Logbook()
    logbook.header = ["gen", "evals"] + stats.fields

    GEN = generations_count
    best = None

    # Results holders for exporting results to CSV file
    # print('Start of evolution')
    for g in range(GEN):
        for part in pop:
            part.fitness.values = toolbox.evaluate(part)
            if not part.best or part.best.fitness < part.fitness:
                part.best = creator.Particle(part)
                part.best.fitness.values = part.fitness.values
            if not best or best.fitness < part.fitness:
                best = creator.Particle(part)
                best.fitness.values = part.fitness.values
        for part in pop:
            toolbox.update(part, best)

        # Gather all the fitnesses in one list and print the stats
        logbook.record(gen=g, evals=len(pop), **stats.compile(pop))
        # print(logbook.stream)
    # print('End of evolution')

    # here best is the same as:
    # best_ind = tools.selBest(pop, 1)[0]
    # best_ind.best

    return pop, logbook, best

def run_gavrptw(instance_name, unit_cost, init_cost, wait_cost, delay_cost, ind_size, pop_size, \
    cx_pb, mut_pb, n_gen, export_csv=False, customize_data=False):
    '''gavrptw.core.run_gavrptw(instance_name, unit_cost, init_cost, wait_cost, delay_cost,
        ind_size, pop_size, cx_pb, mut_pb, n_gen, export_csv=False, customize_data=False)'''
    if customize_data:
        json_data_dir = os.path.join(BASE_DIR, 'data', 'json_customize')
    else:
        json_data_dir = os.path.join(BASE_DIR, 'data', 'json')
    json_file = os.path.join(json_data_dir, f'{instance_name}.json')
    instance = load_instance(json_file=json_file)
    if instance is None:
        return
    creator.create('FitnessMax', base.Fitness, weights=(1.0,))
    creator.create('Individual', list, fitness=creator.FitnessMax)
    toolbox = base.Toolbox()
    # Attribute generator
    toolbox.register('indexes', random.sample, range(1, ind_size + 1), ind_size)
    # Structure initializers
    toolbox.register('individual', tools.initIterate, creator.Individual, toolbox.indexes)
    toolbox.register('population', tools.initRepeat, list, toolbox.individual)
    # Operator registering
    toolbox.register('evaluate', eval_vrptw, instance=instance, unit_cost=unit_cost, \
        init_cost=init_cost, wait_cost=wait_cost, delay_cost=delay_cost)
    toolbox.register('select', tools.selRoulette)
    toolbox.register('mate', cx_partialy_matched)
    toolbox.register('mutate', mut_inverse_indexes)
    pop = toolbox.population(n=pop_size)
    # Results holders for exporting results to CSV file
    csv_data = []
    print('Start of evolution')
    # Evaluate the entire population
    fitnesses = list(map(toolbox.evaluate, pop))
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit
    print(f'  Evaluated {len(pop)} individuals')
    # Begin the evolution
    for gen in range(n_gen):
        print(f'-- Generation {gen} --')
        # Select the next generation individuals
        offspring = toolbox.select(pop, len(pop))
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))
        # Apply crossover and mutation on the offspring
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < cx_pb:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values
        for mutant in offspring:
            if random.random() < mut_pb:
                toolbox.mutate(mutant)
                del mutant.fitness.values
        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit
        print(f'  Evaluated {len(invalid_ind)} individuals')
        # The population is entirely replaced by the offspring
        pop[:] = offspring
        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in pop]
        length = len(pop)
        mean = sum(fits) / length
        sum2 = sum(x*x for x in fits)
        std = abs(sum2 / length - mean**2)**0.5
        print(f'  Min {min(fits)}')
        print(f'  Max {max(fits)}')
        print(f'  Avg {mean}')
        print(f'  Std {std}')
        # Write data to holders for exporting results to CSV file
        if export_csv:
            csv_row = {
                'generation': gen,
                'evaluated_individuals': len(invalid_ind),
                'min_fitness': min(fits),
                'max_fitness': max(fits),
                'avg_fitness': mean,
                'std_fitness': std,
            }
            csv_data.append(csv_row)
    print('-- End of (successful) evolution --')
    best_ind = tools.selBest(pop, 1)[0]
    print(f'Best individual: {best_ind}')
    print(f'Fitness: {best_ind.fitness.values[0]}')
    print_route(ind2route(best_ind, instance))
    print(f'Total cost: {1 / best_ind.fitness.values[0]}')
    if export_csv:
        csv_file_name = f'{instance_name}_uC{unit_cost}_iC{init_cost}_wC{wait_cost}' \
            f'_dC{delay_cost}_iS{ind_size}_pS{pop_size}_cP{cx_pb}_mP{mut_pb}_nG{n_gen}.csv'
        csv_file = os.path.join(BASE_DIR, 'results', csv_file_name)
        print(f'Write to file: {csv_file}')
        make_dirs_for_file(path=csv_file)
        if not exist(path=csv_file, overwrite=True):
            with io.open(csv_file, 'wt', newline='') as file_object:
                fieldnames = [
                    'generation',
                    'evaluated_individuals',
                    'min_fitness',
                    'max_fitness',
                    'avg_fitness',
                    'std_fitness',
                ]
                writer = DictWriter(file_object, fieldnames=fieldnames, dialect='excel')
                writer.writeheader()
                for csv_row in csv_data:
                    writer.writerow(csv_row)
